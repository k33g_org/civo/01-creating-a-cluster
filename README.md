# 01-creating-a-cluster

## Requirements

### Security

You need a Civo API Key: go to [https://dashboard.civo.com/security](https://dashboard.civo.com/security), and copy your key somewhere. Store the value of the key into an environment variable: `CIVO_API_KEY`.

### Tools

You need:

- **kubectl**: 
  - [https://kubernetes.io/docs/reference/kubectl/](https://kubernetes.io/docs/reference/kubectl/)
  - Install: [https://kubernetes.io/docs/tasks/tools/](https://kubernetes.io/docs/tasks/tools/)
- **K9s**: a Kubernetes CLI to manage your clusters: (*this tool is very useful*)
  - [https://k9scli.io/](https://k9scli.io/)
  - Install: [https://k9scli.io/topics/install/](https://k9scli.io/topics/install/)
- **Civo CLI**: you can create a cluster on Civo.com with the administrator web UI. But using the CLI is very straightforward:
  - [https://www.civo.com/docs/overview/civo-cli](https://www.civo.com/docs/overview/civo-cli)
  - I used this:
  ```bash
  curl -sL https://civo.com/get | sh
  sudo mv /tmp/civo /usr/local/bin/civo
  ```

## Create your first cluster

### Initialize the CLI

```bash
# Add the key to the CLI tool
civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
```

### Create the cluster

We are going to create a cluster with this specifications:
- Name: `first-cluster`
- Size: `g4s.kube.xsmall`, use this command to get all the possible sizes: `civo kubernetes size` 
- Nodes: we will use only a single node
- Region: `FRA1` (Frankfurt, Germany ), use this command to get all the regions: `civo region ls`

Type thes commands to create the cluster and wait for around one minute:
```bash
# Create a directory to store the configuration file to connect to the cluster
mkdir -p config
export KUBECONFIG=$PWD/config/k3s.yaml

CLUSTER_NAME="first-cluster"
CLUSTER_NODES=1
CLUSTER_SIZE="g4s.kube.xsmall"
CLUSTER_REGION="FRA1"

# Create the cluster
civo kubernetes create ${CLUSTER_NAME} \
--size=${CLUSTER_SIZE} \
--nodes=${CLUSTER_NODES} \
--region=${CLUSTER_REGION} \
--wait
```

Output:
```bash
Creating a 1 node k3s cluster of g4s.kube.xsmall instances called first-cluster... - 
The cluster first-cluster (2757362e-ff6b-4d5b-9cdc-488cfd2260b3) has been created in 1 min 5 sec
```

When the cluster is created, you can check it ont [https://dashboard.civo.com/kubernetes](https://dashboard.civo.com/kubernetes)

### Grab the Kubernetes config file

```bash
# Get the kubernetes config file
CLUSTER_REGION="FRA1"
CLUSTER_NAME="first-cluster"

civo --region=${CLUSTER_REGION} \
kubernetes config ${CLUSTER_NAME} > ./config/k3s.yaml
```

The content of `k3s.yaml` should look like this:
```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJkekNDQVIyZ0F3SUJBZ0lCQURBS0JnZ
    server: https://74.220.27.108:6443
  name: first-cluster
contexts:
- context:
    cluster: first-cluster
    user: first-cluster
  name: first-cluster
current-context: first-cluster
kind: Config
preferences: {}
users:
- name: first-cluster
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJrVENDQVRlZ0F3SUJBZ0lJUmhtZTFUb
    client-key-data: LS0tLS1CRUdJTiBFQyBQUklWQVRFIEtFWS0tLS0tCk1IY0NBUUVFSU0xUndIMVdTSW0rUmdHa
```

## Some useful commands

### Get information about the cluster

Type these commands to get information about your new cluster:
```bash
# Get the kubernetes config file
CLUSTER_REGION="FRA1"
CLUSTER_NAME="first-cluster"

civo --region=${CLUSTER_REGION} kubernetes show ${CLUSTER_NAME}
```

You should get something like that:
```bash
                    ID : 2757362e-ff6b-4d5b-9cdc-488cfd2260b3
                  Name : first-cluster
           ClusterType : k3s
                Region : FRA1
                 Nodes : 1
                  Size : g4s.kube.xsmall
                Status : ACTIVE
              Firewall : k3s-cluster-first-cluster-4ada-4d51b0
               Version : 1.26.4-k3s1 *
          API Endpoint : https://74.220.27.108:6443
           External IP : 74.220.27.108
          DNS A record : 2757362e-ff6b-4d5b-9cdc-488cfd2260b3.k8s.civo.com
Installed Applications : metrics-server, Traefik-v2-nodeport

* An upgrade to v1.27.1-k3s1 is available. Learn more about how to upgrade: civo k3s upgrade --help

Conditions:
+---------------------------------------+--------+
| Message                               | Status |
+---------------------------------------+--------+
| Control Plane is accessible           | True   |
+---------------------------------------+--------+
| Worker nodes from all pools are ready | True   |
+---------------------------------------+--------+
| Cluster is on desired version         | True   |
+---------------------------------------+--------+

Pool (fa6a80):
+----------------------------------------------------+---------------+--------+-----------------+-----------+----------+---------------+
| Name                                               | IP            | Status | Size            | Cpu Cores | RAM (MB) | SSD disk (GB) |
+----------------------------------------------------+---------------+--------+-----------------+-----------+----------+---------------+
| k3s-first-cluster-5951-4d51b0-node-pool-ec67-dy1v9 | 74.220.27.108 | ACTIVE | g4s.kube.xsmall |         1 |     1024 |            30 |
+----------------------------------------------------+---------------+--------+-----------------+-----------+----------+---------------+

Labels:
kubernetes.civo.com/node-pool=fa6a80a4-b142-4830-8a40-8749604c5dd7
kubernetes.civo.com/node-size=g4s.kube.xsmall

Applications:
+---------------------+-----------+-----------+--------------+
| Name                | Version   | Installed | Category     |
+---------------------+-----------+-----------+--------------+
| metrics-server      | (default) | true      | architecture |
+---------------------+-----------+-----------+--------------+
| Traefik-v2-nodeport | 2.9.4     | true      | architecture |
+---------------------+-----------+-----------+--------------+
```

### Run K9s to connect to the cluster

```bash
export KUBECONFIG=$PWD/config/k3s.yaml
k9s --all-namespaces
```

![k9s](imgs/01-k9s.png)

## First Deployment

> example from [https://kubernetes.io/docs/tutorials/hello-minikube/](https://kubernetes.io/docs/tutorials/hello-minikube/)

To deploy a Docker image of an application to Kubernetes, you must create a deployment with the following command:
```bash
# Run a test container image that includes a webserver
kubectl create deployment hello-node --image=registry.k8s.io/e2e-test-images/agnhost:2.39 -- /agnhost netexec --http-port=8080
```

Output:
```bash
deployment.apps/hello-node created
```

If you run again K9s, you can see a new pod into the `default` namespace:
![k9s-deployment](imgs/02-deployment.png)

Or use this `kubectl` command to see the deployment:
```bash
kubectl get deployments
```

Output:
```bash
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
hello-node   1/1     1            1           4m39s
```

Then, use this `kubectl` command to see the pod:
```bash
kubectl get pods
```

Output:
```bash
NAME                          READY   STATUS    RESTARTS   AGE
hello-node-7b87cd5f68-k7wct   1/1     Running   0          6m34s
```

## First Service

To make the `hello-node` container accessible from outside, you need to expose the Pod as a Kubernetes Service:

Then, use this `kubectl` command to create the service:
```bash
kubectl expose deployment hello-node --type=LoadBalancer --port=8080
```

Output:
```bash
service/hello-node exposed
```

Type the following command to get the list of the deployed services:
```bash
kubectl get services
```

Output:
```bash
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)          AGE
kubernetes   ClusterIP      10.43.0.1       <none>          443/TCP          50m
hello-node   LoadBalancer   10.43.227.233   74.220.28.148   8080:31446/TCP   115s
```

Theoretically, you should be able to "join" your application to this address: `http://74.220.28.148:8080`. Try this:

```bash
curl http://74.220.28.148:8080
```

Output:
```bash
NOW: 2023-09-05 08:36:07.21428834 +0000 UTC m=+967.92923756
```

You see, it doesn't seem too complicated.

## Other useful Civo CLI commands

### Get the list of your clusters

```bash
CLUSTER_REGION="FRA1"
civo --region=${CLUSTER_REGION} kubernetes list
```

Output:
```bash
+--------------------------------------+---------------+--------------+-------+-------+----------------------------------+
| ID                                   | Name          | Cluster-Type | Nodes | Pools | Conditions                       |
+--------------------------------------+---------------+--------------+-------+-------+----------------------------------+
| 2757362e-ff6b-4d5b-9cdc-488cfd2260b3 | first-cluster | k3s          |     1 |     1 | Control Plane Accessible: True   |
|                                      |               |              |       |       | All Workers Up: True             |
|                                      |               |              |       |       | Cluster On Desired Version: True |
|                                      |               |              |       |       |                                  |
+--------------------------------------+---------------+--------------+-------+-------+----------------------------------+
```

### Delete the cluster

To delete the cluser, use the following commands:
```bash
CLUSTER_REGION="FRA1"
CLUSTER_NAME="first-cluster"
civo kubernetes remove ${CLUSTER_NAME} --region=${CLUSTER_REGION} --yes 
```

Output:
```bash
The Kubernetes cluster (first-cluster) has been deleted
```



## Remarks

- With Gitpod and GitLab, to store `CIVO_API_KEY`, use this pattern `*/**` for the scope of the variable.
